@extends('cp.master')

@section('content')
	
	<div class="row">
		<div class="col-md-6 col-md-offset-2">
			<h1 class="page-header">Ubah Call Center</h1>
			@if(session('success-update'))
			   	<div class="alert alert-success">
			       	{{ session('success-update') }}
			   	</div>
			@elseif(session('error-update'))
			   	<div class="alert alert-danger">
			       	{{ session('error-update') }}
			   	</div>
			@endif	

			{!! Form::model($callcenter, ['route' => ['cp.callcenter.update', $callcenter->id], 'method' => 'put']) !!}
				<div class="form-group">
					<label for="instansi">Instansi</label>
					{!! Form::select('instansi', $instansi, $callcenter->institute_id, ['id' => 'instansiPegawai', 'class' => 'form-control']) !!}
					{!! ($errors->first('instansi')) ? $errors->first('instansi', '<span class="help-block">:message</span>') : '' !!}
				</div>
				<div class="form-group">
					<label for="no_telepon">No. Telepon</label>
					{!! Form::text('no_telepon', $callcenter->phone_number, ['class' => 'form-control']) !!}
					{!! ($errors->first('no_telepon') ?  $errors->first('no_telepon', '<span class="help-block">:message</span>') : '') !!}
				</div>

				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Simpan Perubahan">
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection