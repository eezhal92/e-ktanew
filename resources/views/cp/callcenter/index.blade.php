@extends('cp.master')

@section('content')
	
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-header">Call Center <small>data master</small></h1>
			<div class="row">
				<div class="col-md-8">

					@if(session('success-create'))
			        	<div class="alert alert-success">
			            	{{ session('success-create') }}
			          	</div>
			        @elseif(session('error-create'))
			        	<div class="alert alert-danger">
			            	{{ session('error-create') }}
			          	</div>
			         @endif	

			         @if(session('success-delete'))
			        	<div class="alert alert-success">
			            	{{ session('success-delete') }}
			          	</div>
			        @elseif(session('error-delete'))
			        	<div class="alert alert-danger">
			            	{{ session('error-delete') }}
			          	</div>
			         @endif	

					<table class="table table-condensed">
						<thead>
							<tr>
								<th>No. Call Center</th>
								<th width="120px">Aksi</th>
							</tr>
						</thead>
						<tbody>
							
							@foreach($callcenters as $cc)
								
							<tr>
								<td>{{ $cc->phone_number }}</td>
								<td>
									<a href="{{ route('cp.callcenter.edit', $cc->id) }}" class="btn btn-warning btn-xs">Ubah</a>
									<a class="btn btn-danger btn-xs btn-delete" data-title="{{ $cc->phone_number }}" data-url="{{ action('CallCenterController@destroy', $cc->id) }}">Delete</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="col-md-3 col-md-offset-1">
					<div class="panel panel-default">
						<div class="panel-heading">
							Tambah Data
						</div>
						<div class="panel-body">
							{!! Form::open(['route' => 'cp.callcenter.store', 'method' => 'post']) !!}
								<div class="form-group {{ ($errors->first('no_telepon')) ? 'has-error' : '' }}">
									<label for="instansi">Instansi</label>
									{!! Form::select('instansi', $instansi, null, ['id' => 'instansiPegawai', 'class' => 'form-control']) !!}
									{!! ($errors->first('instansi')) ? $errors->first('instansi', '<span class="help-block">:message</span>') : '' !!}
									<label for="no_telepon">No. Telepon</label>
									<input type="text" name="no_telepon" class="form-control">
									{!! ($errors->first('no_telepon')) ? $errors->first('no_telepon', '<span class="help-block">:message</span>') : '' !!}
								</div>
								<div class="form-group">
									<input type="submit" class="btn btn-primary" value="Simpan">
								</div>
							{!! Form::close() !!}
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
@endsection