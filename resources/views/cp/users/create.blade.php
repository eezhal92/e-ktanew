@extends('cp.master')

@section('content')
	
	<div class="row">
		<div class="col-md-6 col-md-offset-2">
			<h1 class="page-header">Menambah Data User</h1>

			{!! Form::open(['route' => 'cp.users.store', 'method' => 'post']) !!}
				
				<div class="form-group {{ ($errors->first('nama')) ? 'has-error' : '' }}">
					<label for="nama">Nama</label>
					{!! Form::text('nama', null, ['class' => 'form-control'])!!}
					{!! ($errors->first('nama')) ? $errors->first('nama', '<span class="help-block">:message</span>') : '' !!}
				</div>

				<div class="form-group {{ ($errors->first('email')) ? 'has-error' : '' }}">
					<label for="email">Email</label>
					{!! Form::text('email', null, ['class' => 'form-control'])!!}
					{!! ($errors->first('email')) ? $errors->first('email', '<span class="help-block">:message</span>') : '' !!}
				</div>

				<div class="form-group {{ ($errors->first('password')) ? 'has-error' : '' }}">
					<label for="password">Password</label>
					{!! Form::password('password', ['class' => 'form-control'])!!}
					{!! ($errors->first('password')) ? $errors->first('password', '<span class="help-block">:message</span>') : '' !!}
				</div>

				<div class="form-group {{ ($errors->first('konfirmasi_password')) ? 'has-error' : '' }}">
					<label for="konfirmasi_password">Konfirmasi Password</label>
					{!! Form::password('konfirmasi_password', ['class' => 'form-control'])!!}
					{!! ($errors->first('konfirmasi_password')) ? $errors->first('konfirmasi_password', '<span class="help-block">:message</span>') : '' !!}
				</div>

				<div class="form-group {{ ($errors->first('level')) ? 'has-error' : '' }}">
					<label for="level">Level</label>
					{!! Form::select('level', ['user'=> 'User', 'admin' => 'Admin'], null, ['class' => 'form-control'])!!}
					{!! ($errors->first('level')) ? $errors->first('level', '<span class="help-block">:message</span>') : '' !!}
				</div>

				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Simpan">
				</div>		

			{!! Form::close() !!}
		</div>
	</div>
@endsection