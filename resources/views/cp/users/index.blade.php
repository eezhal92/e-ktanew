@extends('cp.master')

@section('content')
	
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-header">User <small>data master</small> <a href="{{ route('cp.users.create') }}" class="btn btn-primary pull-right">Tambah Data</a></h1>
			@if(session('success-create'))
			  	<div class="alert alert-success">
			       	{{ session('success-create') }}
			   	</div>
			@elseif(session('error-create'))
			   	<div class="alert alert-danger">
			       	{{ session('error-create') }}
			   	</div>
			@endif	

			@if(session('success-update'))
			   	<div class="alert alert-success">
			       	{{ session('success-update') }}
			   	</div>
			@elseif(session('error-update'))
			   	<div class="alert alert-danger">
			       	{{ session('error-update') }}
			   	</div>
			@endif

			@if(session('success-update'))
			   	<div class="alert alert-success">
			       	{{ session('success-update') }}
			   	</div>
			@elseif(session('error-update'))
			   	<div class="alert alert-danger">
			       	{{ session('error-update') }}
			   	</div>
			@endif			
			
			<table class="table table-condensed table-striped table-hover">
				<thead>
					<tr><th>Nama</th>
						<th>Email</th>						
						<th>Level</th>						
						<th style="width: 120px">Aksi</th>						
					</tr>
				</thead>
				<tbody>

					@foreach($users as $user)
						
						<tr>
							<td>{{ $user->name }}</td>
							<td>{{ $user->email }}</td>
							<td>{{ $user->level }}</td>						
							<td>
								<a href="{{ route('cp.users.edit', $user->id) }}" class="btn btn-warning btn-xs">Ubah</a>
								<a href="#" class="btn btn-danger btn-xs btn-delete" data-url="{{ action('UserController@destroy', $user->id) }}" data-title="{{ $user->name }}">Hapus</a>																
							</td>
						</tr>
					@endforeach
				</tbody>
				{!! $users->render() !!}
			</table>
		</div>
	</div>
@endsection