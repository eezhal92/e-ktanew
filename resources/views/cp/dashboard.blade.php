@extends('cp.master')

@section('content')

	<div class="row">
		<div class="col-md-12">
			<h1 class="page-header">Dashboard</h1>
			<div class="alert alert-info">Selamat Datang di Panel Admin, eKTA Satpol PP. Anda login sebagai <b>{{ Auth::user()->level }}</b></div>				
		</div>
	</div>

	@if(Session::has('error-user'))
		<div class="alert alert-danger">
		{{ Session::get('error-user') }}
		</div>
	@endif

	<div class="row">
		<div class="col-md-4 stats">			
			<div class="panel panel-default">
				<div class="panel-heading">
					Jumlah Pegawai
				</div>
				<div class="panel-body">
					<span class="big"> {{ $pegawai->count() }}</span> orang
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					Jumlah Instansi
				</div>
				<div class="panel-body">
					<span class="big"> {{ $instansi->count() }}</span> instansi
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					Pegawai Berdasarkan Instansi
				</div>
				<div class="panel-body">
					<table class="table">
						<thead>
							<tr>
								<th>Nama</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							@foreach($instansi as $ins)
							<tr>
								<td>{{ $ins->name }}</td>
								<td>{{ $ins->employee->count() }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					Pegawai Berdasarkan Jabatan
				</div>
				<div class="panel-body">
					<table class="table">
						<thead>
							<tr>
								<th>Nama</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							@foreach( $jabatan as $jab )
							<tr>
								<td>{{ $jab->name }}</td>
								<td>{{ $jab->employee->count() }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection