@extends('cp.master')

@section('content')
	
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-header">Pegawai <small>data master</small> <a href="{{ route('cp.pegawai.create') }}" class="btn btn-primary pull-right">Tambah Data</a></h1>

			<form class="form-inline" style="text-align:right;">            		            
                <div class="form-group">
                	<label for="">Cari berdasar</label>
                	{!! Form::select('f_instansi', $instansi, Input::get('f_instansi'),	 ['class' => 'form-control input-sm']) !!}
                </div>
                <div class="form-group">
                	{!! Form::select('f_jabatan', $jabatan, Input::get('f_jabatan'), ['class' => 'form-control input-sm']) !!}
                </div>
                <div class="form-group">
                	{!! Form::select('f_field', ['nama' => 'Nama', 'nip' => 'NIP'], Input::get('f_field'), ['class' => 'form-control input-sm']) !!}
                </div>
	            <div class="form-group">
	                <input type="text" name="query" class="form-control input-sm" placeholder="Masukkan kata kunci" value="{{ Input::get('query') }}">
	            </div>
	            <input type="submit" class="btn btn-primary btn-sm" value="Cari">
	        </form>

	        @if (session('error-delete'))
	          <div class="alert alert-danger">
	            {{ session('error-delete') }}
	          </div>
	        @endif
	        
	        @if (session('success-delete'))
	          <div class="alert alert-success">
	            {{ session('success-delete') }}
	          </div>
	        @endif

			<hr>

			@if(!is_null(Input::get('query')))
				<blockquote>
				  <code>Menemukan {{ $pegawai->count() }} data.</code>
				</blockquote>				
			@endif
			
			<table class="table table-condensed table-striped table-hover">
				<thead>
					<tr>
						<th>NIP</th>
						<th>Nama</th>
						<th>No. HP</th>						
						<th>Jabatan</th>						
						<th>Instansi</th>						
						<th>Alamat</th>						
						<th style="width: 100px">Aksi</th>
						<th style="width: 100px">Lainnya</th>
					</tr>
				</thead>
				<tbody>

					@foreach($pegawai as $peg)
						
						<tr>
							<td>{{ $peg->id_number }}</td>
							<td>{{ $peg->name }}</td>
							<td>{{ $peg->phone_number }}</td>
							<td>{{ $peg->position->name }}</td>
							<td>{{ $peg->institute->name }}</td>
							<td>{{ $peg->address }}</td>
							<td>
								<a href="{{ action('EmployeeController@edit', ['id' => $peg->id]) }}" class="btn btn-warning btn-xs btn-block">Ubah</a>
								<a class="btn btn-danger btn-xs btn-block btn-delete" data-url="{{ action('EmployeeController@destroy', $peg->id) }}" data-title="{{ $peg->name }}">Hapus</a>																
							</td>
							<td>
								<a href="{{ action('EmployeeController@show', ['id' => $peg->id]) }}" class="btn btn-primary btn-xs btn-block">Lihat</a>
								<a href="" class="btn btn-default btn-xs btn-block">Download</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			{!! $pegawai->render() !!}
		</div>
	</div>
@endsection