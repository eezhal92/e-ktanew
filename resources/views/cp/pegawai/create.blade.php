@extends('cp.master')

@section('content')

<div class="row">	
	<div class="col-md-6 col-md-offset-2">
		<h1 class="page-header">Menambah Data Pegawai</h1>
		
		      <!-- Status Check -->
        @if (session('status'))
            <div class="alert alert-success">
              {{ session('status') }}
            </div>
        @endif
        <!-- Status Check -->
	
		{!! Form::open(['route' => 'cp.pegawai.store', 'method' => 'post', 'files' => true]) !!}			

			<div class="form-group {{ ($errors->first('nip')) ? 'has-error' : '' }}">
				<label for="nip">NIP</label>
				{!! Form::text('nip', null, ['class' => 'form-control']) !!}
				{!! ($errors->first('nip')) ? $errors->first('nip', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="form-group {{ ($errors->first('nama')) ? 'has-error' : '' }}">
				<label for="nama">Nama</label>
				{!! Form::text('nama', null, ['class' => 'form-control']) !!}
				{!! ($errors->first('nama')) ? $errors->first('nama', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="form-group {{ ($errors->first('no_telp')) ? 'has-error' : '' }}">
				<label for="no_telp">No. HP</label>
				{!! Form::text('no_telp', null, ['class' => 'form-control']) !!}
				{!! ($errors->first('no_telp')) ? $errors->first('no_telp', '<span class="help-block">:message</span>') : ''!!}			
			</div>

			<div class="form-group {{ ($errors->first('pendidikan')) ? 'has-error' : '' }}">
				<label for="pendidikan">Pendidikan</label>
				{!! Form::select('pendidikan', $pendidikan, null, ['id' => 'pendidikanPegawai', 'class' => 'form-control']) !!}
				{!! ($errors->first('pendidikan')) ? $errors->first('pendidikan', '<span class="help-block">:message</span>') : '' !!}
			</div>
			
			<div class="form-group {{ ($errors->first('instansi')) ? 'has-error' : '' }}">
				<label for="instansi">Instansi</label>
				{!! Form::select('instansi', $instansi, null, ['id' => 'instansiPegawai', 'class' => 'form-control']) !!}
				{!! ($errors->first('instansi')) ? $errors->first('instansi', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="form-group {{ ($errors->first('jabatan')) ? 'has-error' : '' }}">
				<label for="jabatan">Jabatan</label>
				{!! Form::select('jabatan', $jabatan, null, ['id' => 'jabatanPegawai', 'class' => 'form-control']) !!}
				{!! ($errors->first('jabatan')) ? $errors->first('jabatan', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="form-group {{ ($errors->first('golongan')) ? 'has-error' : '' }}">
				<label for="golongan">Golongan</label>
				{!! Form::select('golongan', $golongan, null, ['id' => 'golonganPegawai', 'class' => 'form-control']) !!}
				{!! ($errors->first('golongan')) ? $errors->first('golongan', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="form-group {{ ($errors->first('alamat')) ? 'has-error' : '' }}">
				<label for="alamat">Alamat</label>
				{!! Form::textarea('alamat', null, ['rows' => 3, 'class' => 'form-control']) !!}
				{!! ($errors->first('alamat')) ? $errors->first('alamat', '<span class="help-block">:message</span>') : '' !!}
			</div>
			
			<div class="form-group {{ ($errors->first('foto1')) ? 'has-error' : '' }}">
				<label for="foto1">Foto 1</label>
				<input type="file" name="foto1">
				{!! ($errors->first('foto1')) ? $errors->first('foto1', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="form-group {{ ($errors->first('foto2')) ? 'has-error' : '' }}">
				<label for="foto2">Foto 2</label>
				<input type="file" name="foto2">
				{!! ($errors->first('foto2')) ? $errors->first('foto2', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="form-group {{ ($errors->first('file')) ? 'has-error' : '' }}">
				<label for="file">File</label>
				<input type="file" name="file">
				{!! ($errors->first('file')) ? $errors->first('file', '<span class="help-block">:message</span>') : '' !!}
			</div>	
			
			<hr>
			
			<div class="form-group">				
				<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
			</div>
			
		{!! Form::close() !!}
	</div>
</div>

@endsection