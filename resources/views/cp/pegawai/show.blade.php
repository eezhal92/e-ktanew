@extends('cp.master')

@section('content')

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h1 class="page-header">Detail Pegawai <a href="{{ route('front.pegawai.show', $pegawai->id_number) }}" class="btn btn-default pull-right"><i class="glyphicon glyphicon-eye-open"></i> Lihat Live</a></h1>
			<div class="row">
				<div class="col-md-3 col-md-offset-3">
					<div class="thumbnail">
						<img src="{{ asset($pegawai->photo1) }}" alt="" class="img img-responsive img-rounded">
					</div>		
				</div>
				<div class="col-md-3">					
					<div class="thumbnail">
						<img src="{{ asset($pegawai->photo2) }}" alt="" class="img img-responsive img-rounded">
					</div>			
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<h3>{{ $pegawai->name }}</h3>
					<table class="table">
						<tbody style="font-weight: bold;">
							<tr>
								<td>NIP</td>
								<td>{{ $pegawai->id_number }}</td>
							</tr>
							<tr>
								<td>No. HP</td>
								<td>{{ $pegawai->phone_number }}</td>
							</tr>
							<tr>
								<td>alamat</td>
								<td>{{ $pegawai->address }}</td>
							</tr>
							<tr>
								<td>golongan</td>
								<td>{{ $pegawai->rank->rank }}</td>
							</tr>
							<tr>
								<td>Jabatan</td>
								<td>{{ $pegawai->position->name }}</td>
							</tr>
							<tr>
								<td>Instansi</td>
								<td>{{ $pegawai->institute->name }} <br> <i>Alamat: {{ $pegawai->institute->address }}</i></td>
							</tr>
							<tr>
								<td>pendidikan</td>
								<td>{{ $pegawai->academicDegree->name }}</td>
							</tr>
							<tr>
								<td>File</td>
								<td><a href="{{ asset($pegawai->file) }}">{{ asset($pegawai->file) }}</a></i></td>
							</tr>
						</tbody>
					</table>	
				</div>				
			</div>
		</div>
	</div>
@stop