<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Staff Card</title>
	<style>
		.container {
			width: 960px;
			margin: 0 auto;
		}
	</style>
</head>
<body>
	<div class="container">
		<h1>Hello {{ $word }}</h1>
	</div>
</body>
</html>