@extends('cp.master')

@section('content')
	
	<div class="row">
		<div class="col-md-6 col-md-offset-2">
			<h1 class="page-header">Edit Data Jabatan</h1>

			@if(session('success-update'))
		      	<div class="alert alert-success">
		           	{{ session('success-update') }}
		       	</div>
		    @elseif(session('error-update'))
		       	<div class="alert alert-danger">
		          	{{ session('error-update') }}
		       	</div>
		    @endif

			{!! Form::model($jabatan, ['route' => ['cp.jabatan.update', $jabatan->id], 'method' => 'put']) !!}				

				<div class="form-group">
					<label for="nama">Nama</label>
					{!! Form::text('nama', $jabatan->name, ['class' => 'form-control']) !!}
				</div>

				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Simpan Perubahan">
				</div>
				
			{!! Form::close() !!}
		</div>
	</div>
@endsection