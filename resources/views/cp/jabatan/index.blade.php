@extends('cp.master')

@section('content')
	
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-header">Jabatan <small>data master</small></h1>
			
			<div class="row">
				
				<div class="col-md-8">
					@if(session('success-create'))
			        	<div class="alert alert-success">
			            	{{ session('success-create') }}
			          	</div>
			        @elseif(session('error-create'))
			        	<div class="alert alert-danger">
			            	{{ session('error-create') }}
			          	</div>
			         @endif		

			         @if(session('success-delete'))
			        	<div class="alert alert-success">
			            	{{ session('success-delete') }}
			          	</div>
			        @elseif(session('error-delete'))
			        	<div class="alert alert-danger">
			            	{{ session('error-delete') }}
			          	</div>
			         @endif		

					<table class="table table-condensed">
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama</th>						
								<th style="width: 120px;">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; ?>
							@foreach($jabatan as $jab)
								
								<tr>
									<td>{{ $no }}</td>
									<td>{{ $jab->name }}</td>
									<td>
										<a href="{{ route('cp.jabatan.edit', $jab->id) }}" class="btn btn-warning btn-xs">Ubah</a>
										<a href="#" class="btn btn-danger btn-xs btn-delete" data-url="{{ action('PositionController@destroy', $jab->id) }}" data-title="{{ $jab->name }}">Hapus</a>
									</td>
								</tr>
								<?php $no++; ?>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="col-md-3 col-md-offset-1">
					<div class="panel panel-default">
						<div class="panel-heading">
							Tambah Data
						</div>
						<div class="panel-body">
							<form action="{{ route('cp.jabatan.store') }}" method="post">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="form-group {{ ($errors->first('nama') ? 'has-error' : '') }}">
									<label for="">Nama</label>
									<input type="text" class="form-control" name="nama" placeholder="Nama Jabatan">
									{{ ($errors->first('nama')) ? $errors->first('nama', '<span class="help-block">:message</span>') : '' }}
								</div>
								<div class="form-group">					
									<input type="submit" class="btn btn-primary" value="Simpan">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			
		</div>		
	</div>
@endsection