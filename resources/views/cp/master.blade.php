<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_csrf_token" content="{{ csrf_token() }}">
	
	<title>eKTA Satpol PP</title>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{ asset('css/main.css') }}">
	<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
	<style>
		@import url(http://fonts.googleapis.com/css?family=Roboto:400,500,700);
		
		body {
			font-family: 'Roboto', sans-serif;
		}

		.stats span.big {
			font-size: 30px;
			margin-right: 7px;
		}
	</style>
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<section id="top-navbar">
		@include('components.top-navbar')
	</section>
	
	@if(Session::has('error') || Session::has('success'))
		@include('components.alert')
	@endif

	<section id="main-content">
		<div class="container">
			@yield('content')
		</div>
	</section>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>	
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>	
	<script src="{{ asset('js/select2.min.js') }}"></script>
	<script src="{{ asset('js/main.js') }}"></script>
</body>
</html>