@extends('cp.master')

@section('content')
	
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-header">Instansi <small>data master</small> <a href="{{ route('cp.instansi.create') }}" class="btn btn-primary pull-right">Tambah Data</a></h1>
			
	        @if (session('error-delete'))
	          <div class="alert alert-danger">
	            {{ session('error-delete') }}
	          </div>
	        @endif
	        @if (session('success-delete'))
	          <div class="alert alert-success">
	            {{ session('success-delete') }}
	          </div>
	        @endif

	        @if(session('success-create'))
	        	<div class="alert alert-success">
	            	{{ session('success-create') }}
	          	</div>
	        @elseif(session('error-create'))
	        	<div class="alert alert-danger">
	            	{{ session('error-create') }}
	          	</div>
	         @endif

			<table class="table table-condensed table-striped table-hover">
				<thead>
					<tr>
						<th>Nama Instansi</th>
						<th>Alamat</th>
						<th>Nomor Telepon</th>
						<th style="width: 100px">Aksi</th>
					</tr>
				</thead>
				<tbody>

					@foreach($instansi as $ins)
						
						<tr>
							<td>{{ $ins->name }}</td>
							<td>{{ $ins->address }}</td>
							<td>{{ $ins->phone_number }}</td>
							<td>
								<a href="{{ action('InstituteController@edit', ['id' => $ins->id]) }}" class="btn btn-warning btn-xs btn-block">Ubah</a>
								<a class="btn btn-danger btn-xs btn-block btn-delete" data-url="{{ action('InstituteController@destroy', $ins->id) }}" data-title="{{ $ins->name }}">Hapus</a>																
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			{!! $instansi->render() !!}
		</div>
	</div>
@endsection