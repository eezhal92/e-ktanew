@extends('cp.master')

@section('content')

<div class="row">	
	<div class="col-md-6 col-md-offset-2">
		<h1 class="page-header">Mengubah Data Instansi</h1>
		
		@if(Session::has('error'))
			<div class="alert alert-danger">
				{{ Session::get('error') }}
			</div>
		@endif

	    @if(session('success-update'))
	      	<div class="alert alert-success">
	           	{{ session('success-update') }}
	       	</div>
	    @elseif(session('error-update'))
	       	<div class="alert alert-danger">
	          	{{ session('error-update') }}
	       	</div>
	    @endif
	
		{!! Form::model($instansi, ['route' => ['cp.instansi.update', $instansi->id], 'method' => 'put', 'files' => true]) !!}

			<div class="form-group {{ ($errors->first('nama')) ? 'has-error' : '' }}">
				<label for="nama">Nama</label>
				{!! Form::text('nama', $instansi->name, ['class' => 'form-control']) !!}
				{!! ($errors->first('nama')) ? $errors->first('nama', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="form-group {{ ($errors->first('no_telp')) ? 'has-error' : '' }}">
				<label for="no_telp">No. Telepon</label>
				{!! Form::text('no_telp', $instansi->phone_number, ['class' => 'form-control']) !!}
				{!! ($errors->first('no_telp')) ? $errors->first('no_telp', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="form-group {{ ($errors->first('alamat')) ? 'has-error' : '' }}">
				<label for="alamat">Alamat</label>
				{!! Form::textarea('alamat', $instansi->address, ['rows' => 3, 'class' => 'form-control']) !!}
				{!! ($errors->first('alamat')) ? $errors->first('alamat', '<span class="help-block">:message</span>') : '' !!}
			</div>
			
			<hr>
			
			<div class="form-group">				
				<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan Perubahan</button>
			</div>
			
		{!! Form::close() !!}
	</div>
</div>

@endsection