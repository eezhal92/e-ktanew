@extends('cp.master')

@section('content')

<div class="row">	
	<div class="col-md-6 col-md-offset-2">
		<h1 class="page-header">Menambah Data Instansi</h1>
		
		      <!-- Status Check -->
        @if (session('status'))
            <div class="alert alert-success">
              {{ session('status') }}
            </div>
        @endif
        <!-- Status Check -->
	
		{!! Form::open(['route' => 'cp.instansi.store', 'method' => 'post', 'files' => true]) !!}			

			<div class="form-group {{ ($errors->first('nama')) ? 'has-error' : '' }}">
				<label for="nama">Nama</label>
				{!! Form::text('nama', null, ['class' => 'form-control']) !!}
				{!! ($errors->first('nama')) ? $errors->first('nama', '<span class="help-block">:message</span>') : '' !!}
			</div>

			<div class="form-group {{ ($errors->first('no_telp')) ? 'has-error' : '' }}">
				<label for="no_telp">No. Telepon</label>
				{!! Form::text('no_telp', null, ['class' => 'form-control']) !!}
				{!! ($errors->first('no_telp')) ? $errors->first('no_telp', '<span class="help-block">:message</span>') : ''!!}			
			</div>

			<div class="form-group {{ ($errors->first('alamat')) ? 'has-error' : '' }}">
				<label for="alamat">Alamat</label>
				{!! Form::textarea('alamat', null, ['rows' => 3, 'class' => 'form-control']) !!}
				{!! ($errors->first('alamat')) ? $errors->first('alamat', '<span class="help-block">:message</span>') : '' !!}
			</div>
			
			<hr>
			
			<div class="form-group">				
				<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
			</div>
			
		{!! Form::close() !!}
	</div>
</div>

@endsection