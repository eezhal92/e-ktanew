@extends('front.layout')

@section('banner')
	
	<!-- Banner -->
	<section id="banner">
		<h2>e-KTA Satuan Polisi Pamong Praja</h2>
		<p>Identifikasi Cepat & Akurat</p>
		<ul class="actions">
			<li><a href="#" class="button special">Sign Up</a></li>
			<li><a href="#" class="button">Learn More</a></li>
		</ul>
	</section>
@stop

@section('sections')
	
	<!-- Main -->
		<section id="main" class="container">

			<section class="box special">
				<header class="major">
					<h2>Memperkenalkan e-KTA
					<!-- <br />
					Satuan Polisi Pamong Praja --></h2>
					<p>Identifikasi keanggotaan Satuan Polisi Pamong Praja dengan cepat dan akurat.</p>
				</header>
				<!-- <span class="image featured"><img src="images/pic01.jpg" alt="" /></span> -->
			</section>

			<section class="box special features">
				<div class="features-row">
					<section>
						<span class="icon major fa-bolt accent2"></span>
						<h3>Respon Cepat</h3>
						<p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
					</section>
					<section>
						<span class="icon major fa-area-chart accent3"></span>
						<h3>Data Akurat</h3>
						<p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
					</section>
				</div>
				<div class="features-row">
					<section>
						<span class="icon major fa-cloud accent4"></span>
						<h3>Akses Kapan Saja, Dimana Saja</h3>
						<p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
					</section>
					<section>
						<span class="icon major fa-lock accent5"></span>
						<h3>Aman dan Terintegratsi</h3>
						<p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
					</section>
				</div>
			</section>

			<!-- <div class="row">
				<div class="6u 12u(narrower)">

					<section class="box special">
						<span class="image featured"><img src="images/pic02.jpg" alt="" /></span>
						<h3>Sed lorem adipiscing</h3>
						<p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
						<ul class="actions">
							<li><a href="#" class="button alt">Learn More</a></li>
						</ul>
					</section>

				</div>
				<div class="6u 12u(narrower)">

					<section class="box special">
						<span class="image featured"><img src="images/pic03.jpg" alt="" /></span>
						<h3>Accumsan integer</h3>
						<p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
						<ul class="actions">
							<li><a href="#" class="button alt">Learn More</a></li>
						</ul>
					</section>

				</div> -->
			</div>

		</section>

	<!-- CTA -->
		<section id="cta">

				@if(Session::has('error'))
					<div class="alert alert-danger">
					{{ Session::get('error') }}
					</div>
				@endif

			<h2>Cari Berdasarkan NIP</h2>
			<p>Blandit varius ut praesent nascetur eu penatibus nisi risus faucibus nunc.</p>

			<form method="post" action="{{ route('front.pegawai.search') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="row uniform 50%">
					<div class="8u 12u(mobilep)">
						<input type="text" name="nip" id="email" placeholder="NIP Pegawai" />
					</div>
					<div class="4u 12u(mobilep)">
						<input type="submit" value="Cari" class="fit" />
					</div>
				</div>
			</form>

		</section>

@stop