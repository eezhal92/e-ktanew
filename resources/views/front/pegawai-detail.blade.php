@extends('front.layout')

@section('sections')

	<!-- Main -->
		@if($pegawai)
		<section id="main" class="container">	
			<header>
				<h2>{{ $pegawai->name }}</h2>
			</header>
			<div class="box">
				<table>
					<tbody>
						<tr>
							<td>NIP</td>
							<td>{{ $pegawai->id_number }}</td>							
						</tr>
						<tr>
							<td>No. HP</td>
							<td>{{ $pegawai->phone_number }}</td>							
						</tr>
						<tr>
							<td>Instansi</td>
							<td>{{ $pegawai->institute->name }}</td>							
						</tr>
						<tr>
							<td>Alamat Instansi</td>
							<td>{{ $pegawai->institute->address }}</td>							
						</tr>
						<tr>
							<td>Callcenter</td>
							<td>{{ $cc->phone_number }}</td>							
						</tr>
					</tbody>					
				</table>
				<div class="row">
					<div class="6u 12u(mobilep)">
						<h3>Foto 1</h3>
						<img src="{{ asset($pegawai->photo1) }}" width="120" height="180" alt="">
					</div>
					<div class="6u 12u(mobilep)">
						<h3>Foto 2</h3>
						<img src="{{ asset($pegawai->photo2) }}" width="120" height="180" alt="">
					</div>
					<div class="6u 12u(mobilep)">
						<h3>QRCode</h3>
						{!! QrCode::size(200)->generate($all); !!}
					</div>
				</div>
			</div>
		</section>
	@else
		<section id="main" class="container">	
			<header>
				<h2>Oooops...</h2>
			</header>
			<div class="box">
				<div class="alert alert-danger">
					Maaf, pegawai dengan NIP <b>{{ $nip }}</b> tidak ditemukan
				</div>
			</div>
		</section>
	@endif
@stop