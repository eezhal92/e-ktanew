$(document).ready(function() {

	$('#jabatanPegawai').select2();
	$('#instansiPegawai').select2();

	 $('.btn-delete').on('click', function() {
        var url 	= $(this).data('url');
        var token 	= $('meta[name="_csrf_token"]').attr('content');
        var title 	= $(this).data('title');

        console.log(url);
        console.log(title);
        console.log(token);

        var conf = confirm("Apakah Anda yakin menghapus " + title + " ?");

        if(conf) {
        	$.ajax({
                method: 'post',
                url: url,
                data: {_token: token, _method: 'delete'}
            }).done(function(res) {
                alert(res.success);
                window.location.reload();
            }).fail(function(res) {
                alert(res.responseText);
            });
        }

        return false;
    });

});
