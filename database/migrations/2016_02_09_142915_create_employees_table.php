<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_number');
            $table->string('name');
            $table->string('phone_number');
            $table->text('address');
            $table->integer('academic_degree_id');
            $table->integer('position_id');
            $table->integer('institute_id');
            $table->integer('rank_id');
            $table->string('photo1');
            $table->string('photo2');
            $table->string('file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees');
    }
}
