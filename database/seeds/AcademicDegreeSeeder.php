<?php

use App\AcademicDegree;
use Illuminate\Database\Seeder;

class AcademicDegreeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AcademicDegree::truncate();

        $degrees = [
            'Tidak Sekolah', 'TK', 'SD', 'SMP', 
            'SMA', 'D3', 'S1', 'S2', 'S3'
        ];

        foreach ($degrees as $degree) {
            AcademicDegree::create(['name' => $degree]);
        }
    }
}
