<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(AcademicDegreeSeeder::class);
        $this->call(RankSeeder::class);
        $this->call(InstituteSeeder::class);
        $this->call(PositionSeeder::class);
        $this->call(CallCenterSeeder::class);

        Model::reguard();
    }
}
