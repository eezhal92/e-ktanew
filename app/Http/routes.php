<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



// Front Routes
Route::get('/', 'HomeController@index');

Route::post('/front-pegawai/search', [
    'as'  => 'front.pegawai.search',
    'uses'  => function() {
        $nip = Input::get('nip');

        return redirect(route('front.pegawai.show', $nip));
    }
]);

Route::get('/front-pegawai/{nip}', [
    'as'  => 'front.pegawai.show',
    'uses'  => 'HomeController@getPegawai'
]);


Route::group(['prefix' => 'cp', 'middleware' => 'auth'], function () {
	
	Route::get('dashboard', [
		'as' 	=> 	'cp.dashboard',
		'uses' 	=>	'PanelController@dashboard'
	]);

	Route::resource('pegawai', 'EmployeeController');

    Route::get('pegawai/{id}/file', [
        'as' => 'cp.pegawai.file',
        'uses' => 'EmployeeController@getFile'
        ]);

    Route::resource('instansi', 'InstituteController');

    Route::resource('jabatan', 'PositionController');

    Route::resource('callcenter', 'CallCenterController');

    Route::resource('users', 'UserController');

});

// Authentication Routes

Route::get('auth/login', 'Auth\AuthController@getLogin');

Route::post('auth/login', 'Auth\AuthController@postLogin');

Route::get('auth/logout', 'Auth\AuthController@getLogout');

