<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Position;

class PositionController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jabatan = Position::paginate(10);

        return view('cp.jabatan.index', compact('jabatan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->Validate($request, [
                'nama'  =>   'required | min : 3',
            ]);

        $jabatan = new Position;

        $jabatan->name = $request->nama;

        if($jabatan->save()) {

            return redirect(action('PositionController@index'))->with('success-create', 'Data jabatan berhasil ditambah');

        } else {

            return redirect(action('PositionController@index'))->with('error-create', 'Data jabatan gagal ditambah');

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $jabatan = Position::whereId($id)->firstOrFail();

        return view('cp.jabatan.edit', compact('jabatan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->Validate($request, [
            'nama'  =>  'required | min : 3',
            ]);

        $jabatan = Position::whereId($id)->firstOrFail();

        $jabatan->name = $request->nama;

        if($jabatan->save()){
            return redirect(action('PositionController@edit', $jabatan->id))->with('success-update', 'Data jabatan berhasil diubah');
        };

        return redirect(action('PositionController@edit', $jabatan->id))->with('error-update', 'Data jabatan gagal diubah');

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jabatan = Position::find($id);

        if ($jabatan->delete) {

            return redirect(action('PositionController@index'))->with('success-delete', 'Data berhasil dihapus');
        }

        return redirect(action('PositionController@index'))->with('success-delete', 'Data gagal dihapus');
    }
}
