<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Employee;
use App\Institute;
use App\Position;
use App\AcademicDegree;
use App\Rank;
use DB;
use File;
use Input;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $pegawai = Employee::orderBy('created_at', 'desc');

        if(!is_null(Input::get('query'))) {
            $pegawai = Employee::all();
            $query = Input::get('query');
            $field = Input::get('f_field');

            if($field == 'nip') {
                $pegawai = Employee::where('employees.id_number', 'LIKE', '%'. $query . '%');
            } else if ($field == 'nama') {
                $pegawai =  Employee::where('employees.name', 'LIKE', '%'. $query . '%');
            }
        }

        if($instansi_id = Input::get('f_instansi')) { 
            if($instansi_id !== '0') {
                $pegawai = Employee::where('institute_id', $instansi_id);
            }
        }

        if($jabatan_id = Input::get('f_jabatan')) {
        
            if($jabatan_id !== '0') {
                $pegawai = Employee::where('position_id', $jabatan_id);
            }

        }

        $all_instansi = array('0' => 'Semua Instansi');

        $instansi = Institute::lists('name', 'id');

        $instansi = $instansi->toArray();

        $instansi = array_merge($all_instansi, $instansi);

        $all_jabatan = array('0' => 'Semua Jabatan');

        $jabatan = Position::lists('name', 'id');

        $jabatan = $jabatan->toArray();

        $jabatan = array_merge($all_jabatan, $jabatan);

        $pegawai = $pegawai->paginate(20);

       return view('cp.pegawai.index', compact('pegawai', 'instansi', 'jabatan'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $instansi   = Institute::lists('name', 'id');
        $jabatan    = Position::lists('name', 'id');
        $pendidikan = AcademicDegree::lists('name', 'id');
        $golongan   = Rank::lists('rank', 'id');

        return view('cp.pegawai.create', compact('instansi', 'jabatan', 'pendidikan', 'golongan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->Validate($request, [
            'nip'             =>  "required | min:3",
            'nama'            =>  "required | min:3",
            'no_telp'         =>  "required | min:3",
            'pendidikan'      =>  "required",
            'instansi'        =>  "required",
            'jabatan'         =>  "required",
            'golongan'        =>  "required",
            'alamat'          =>  "required | min:3",
            'foto1'           =>  "required | mimes:jpeg,jpg,png,gif | max:500",
            'foto2'           =>  "required | mimes:jpeg,jpg,png,gif | max:500",
            'file'            =>  "required | max:2048"
            ]);

        $pegawai = new Employee;

        $pegawai->id_number             =   $request->nip;
        $pegawai->name                  =   $request->nama;
        $pegawai->phone_number          =   $request->no_telp;
        $pegawai->academic_degree_id    =   $request->pendidikan;
        $pegawai->institute_id          =   $request->instansi;
        $pegawai->position_id           =   $request->jabatan;
        $pegawai->rank_id               =   $request->golongan;
        $pegawai->address               =   $request->alamat;

        try {

            $foto1 = $request->file('foto1');
            $foto2 = $request->file('foto2');
            $file = $request->file('file');

            $folder = 'ekta/photos';
            $folderFile = 'ekta/files';
            $pathFoto   = public_path($folder);
            $pathFile   = public_path($folderFile);

            // Proses Foto1
            $nama_foto1 = $request->nip.'.'.$foto1->getClientOriginalExtension();
            $pegawai->photo1 = $folder.'/'.$nama_foto1;
            $foto1->move($pathFoto, $nama_foto1);

            // Proses Foto2
            $nama_foto2 = $request->nip.'(2).'.$foto2->getClientOriginalExtension();
            $pegawai->photo2 = $folder.'/'.$nama_foto2;
            $foto2->move($pathFoto, $nama_foto2);
            
            // Proses File
            $nama_file = $request->nip.'-file.'.$file->getClientOriginalExtension();
            $pegawai->file = $folderFile.'/'.$nama_file;
            $file->move($pathFile, $nama_file);

            $pegawai->save();

        } catch(Exception $e) {
            return redirect(action('EmployeeController@create'))->with('status', 'Data pegawai gagal ditambahkan');
            }

        return redirect(action('EmployeeController@create'))->with('status', 'Data pegawai berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pegawai = Employee::find($id);

        return view('cp.pegawai.show', compact('pegawai'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pegawai    = Employee::whereId($id)->firstOrFail();
        $instansi   = Institute::lists('name', 'id');
        $jabatan    = Position::lists('name', 'id');
        $pendidikan = AcademicDegree::lists('name', 'id');
        $golongan   = Rank::lists('rank', 'id');

        // dd($pegawai);

        return view('cp.pegawai.edit', compact('pegawai', 'instansi', 'jabatan', 'pendidikan', 'golongan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->Validate($request, [
            'nip'             =>  "required | min:3",
            'nama'            =>  "required | min:3",
            'no_telp'         =>  "required | min:3",
            'pendidikan'      =>  "required",
            'instansi'        =>  "required",
            'jabatan'         =>  "required",
            'golongan'        =>  "required",
            'alamat'          =>  "required | min:3",
            'foto1'           =>  "mimes:jpeg,jpg,png,gif | max:500",
            'foto2'           =>  "mimes:jpeg,jpg,png,gif | max:500",
            'file'            =>  "max:2048"
            ]);

        $pegawai = Employee::whereId($id)->firstOrFail();

        $pegawai->id_number             =   $request->nip;
        $pegawai->name                  =   $request->nama;
        $pegawai->phone_number          =   $request->no_telp;
        $pegawai->academic_degree_id    =   $request->pendidikan;
        $pegawai->institute_id          =   $request->instansi;
        $pegawai->position_id           =   $request->jabatan;
        $pegawai->rank_id               =   $request->golongan;
        $pegawai->address               =   $request->alamat;

        $foto1 = $request->file('foto1');
        $foto2 = $request->file('foto2');
        $file = $request->file('file');

        $folder = 'ekta/photos';
        $folderFile = 'ekta/files';
        $pathFoto   = public_path($folder);
        $pathFile   = public_path($folderFile);


        // proses Foto 1
        if($request->hasFile('foto1')) {
            try {

                $pathFoto1 = public_path('ekta/photos');

                if(File::exists($pathFoto1)) {
                    File::delete($pathFoto1);
                }

                $nama_foto1 = $request->nip.'.'.$foto1->getClientOriginalExtension();
                $pegawai->photo1 = $folder.'/'.$nama_foto1;
                $foto1->move($pathFoto, $nama_foto1);

            } catch(Exception $e) {

                return back()->with('error', $e->getMessage())->withInput();  

            }
        }

        // proses Foto 2
        if($request->hasFile('foto2')) {
            try {

                $pathFoto2 = public_path('ekta/photos');

                if(File::exists($pathFoto2)) {
                    File::delete($pathFoto2);
                }

                $nama_foto2 = $request->nip.'(2).'.$foto2->getClientOriginalExtension();
                $pegawai->photo2 = $folder.'/'.$nama_foto2;
                $foto2->move($pathFoto, $nama_foto2);

            } catch(Exception $e) {

                return back()->with('error', $e->getMessage())->withInput();

            }
        }

        // proses File
        if($request->hasFile('file')) {
            try {

                $pathFile = public_path('ekta/files');                

                if(File::exists($pathFile)) {
                    File::delete($pathFile);
                }

                $nama_file = $request->nip.'-file.'.$file->getClientOriginalExtension();
                $pegawai->file = $folderFile.'/'.$nama_file;                
                $file->move($pathFile, $nama_file);

            } catch(Exception $e) {

                return back()->with('error', $e->getMessage())->withInput();                

            }
        }

        // dd($pegawai);

        if($pegawai->save()) {
            return redirect(action('EmployeeController@edit', $pegawai->id))->with('success', 'Berhasil mengubah data pegawai.');
        } else {
            return redirect(action('EmployeeController@edit', $pegawai->id))->with('error', 'Gagal mengubah data pegawai.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);

        try {

                $pegawai = Employee::whereId($id)->firstOrFail();
                
                $pathFoto1 = public_path($pegawai->photo1);
                $pathFoto2 = public_path($pegawai->photo2);
                $pathFile = public_path($pegawai->file);

                if(File::exists($pathFoto1)) {
                   File::delete($pathFoto1);
                }

                if(File::exists($pathFoto2)) {
                   File::delete($pathFoto2);
                }

                if(File::exists($pathFile)) {
                   File::delete($pathFile);
                }

                $pegawai->delete();
        } catch (Exception $e) {

            return redirect(action('EmployeeController@index'))->with('error-delete', 'Data Pegawai '.$pegawai->name.' Gagal dihapus');

        }

        return redirect(action('EmployeeController@index'))->with('success-delete', 'Data Pegawai '.$pegawai->name.' Berhasil dihapus');
       

    }

    public function getFile($id)
    {
        $pegawai = Employee::find($id);

        return Response::download(public_path($pegawai->file));
    }

}
