<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CallCenter;
use App\Institute;

class CallCenterController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $callcenters = CallCenter::all();
        $instansi = Institute::lists('name', 'id');

        return view('cp.callcenter.index', compact('callcenters', 'instansi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->Validate($request, [
            'no_telepon' => 'required | min : 3',
            ]);

        $callcenter = new CallCenter;

        $callcenter->institutes_id = $request->instansi;
        $callcenter->phone_number = $request->no_telepon;

        if ($callcenter->save()) {

            return redirect(action('CallCenterController@index'))->with('success-create', 'Data call center berhasil ditambah');    

        }

        return redirect(action('CallCenterController@index'))->with('error-create', 'Data call center gagal ditambah');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $callcenter = CallCenter::whereId($id)->firstOrFail();

        $instansi = Institute::lists('name', 'id');

        return view('cp.callcenter.edit', compact('callcenter', 'instansi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->Validate($request, [
            'no_telepon' => 'required | min : 3',
            ]);

        $callcenter = CallCenter::whereId($id)->firstOrFail();

        $callcenter->institutes_id = $request->instansi;
        $callcenter->phone_number = $request->no_telepon;

        if ($callcenter->save()){
        
            return redirect(action('CallCenterController@edit', $callcenter->id))->with('success-update', 'Data call center berhasil diubah');

        }

            return redirect(action('CallCenterController@edit', $callcenter->id))->with('error-update', 'Data call center gagal diubah');
           
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cc = CallCenter::find($id);

        if($cc->delete()) {

            return redirect(action('CallCenterController@index'))->with('success-delete', 'Data berhasil dihapus');
        }

        return redirect(action('CallCenterController@index'))->with('error-delete', 'Data gagal dihapus');
    }
}
