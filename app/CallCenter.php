<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallCenter extends Model
{
    protected $fillable = ['phone_number'];

    public function institute()
    {
    	return $this->hasOne('App\Institute');
    }

}
