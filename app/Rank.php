<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rank extends Model
{
    protected $fillable = ['rank', 'name'];

    public function employee()
    {
    	return $this->hasMany('App\Employee');
    }

}
