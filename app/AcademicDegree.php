<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademicDegree extends Model
{
	protected $table = "academic_degrees";

    protected $fillable = ['name'];

    public function employee()
    {
    	return $this->hasMany('App\Employee');
    }
}
